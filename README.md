collaborative-playlist-views
============================

An idea how to improve collaborative playlists. Made public to be in the public domain uncopyrightable and patentable by anybody, but the author.  
An attempted implementation of an [idea](https://docs.google.com/document/d/1lvY537b8gSbE4n9T8fnSjYM9SdPRt8OSx-ljmpWv0Eg) I had.
